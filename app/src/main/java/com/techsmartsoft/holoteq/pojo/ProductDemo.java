package com.techsmartsoft.holoteq.pojo;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Network model class as well as Rood Database Table class
 */
@Entity(tableName = "ProductDemoTable")
public class ProductDemo {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int productId;
    private String productName;
    private String defaultUrl;
    private float defaultNewPrice;
    private float defaultOldPrice;
    private int defaultUnitQty;
    private String unitText;

    public ProductDemo(int productId, String productName, String defaultUrl, float defaultNewPrice, float defaultOldPrice, int defaultUnitQty, String unitText) {
        this.productId = productId;
        this.productName = productName;
        this.defaultUrl = defaultUrl;
        this.defaultNewPrice = defaultNewPrice;
        this.defaultOldPrice = defaultOldPrice;
        this.defaultUnitQty = defaultUnitQty;
        this.unitText = unitText;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDefaultUrl() {
        return defaultUrl;
    }

    public void setDefaultUrl(String defaultUrl) {
        this.defaultUrl = defaultUrl;
    }

    public float getDefaultNewPrice() {
        return defaultNewPrice;
    }

    public void setDefaultNewPrice(float defaultNewPrice) {
        this.defaultNewPrice = defaultNewPrice;
    }

    public float getDefaultOldPrice() {
        return defaultOldPrice;
    }

    public void setDefaultOldPrice(float defaultOldPrice) {
        this.defaultOldPrice = defaultOldPrice;
    }

    public int getDefaultUnitQty() {
        return defaultUnitQty;
    }

    public void setDefaultUnitQty(int defaultUnitQty) {
        this.defaultUnitQty = defaultUnitQty;
    }

    public String getUnitText() {
        return unitText;
    }

    public void setUnitText(String unitText) {
        this.unitText = unitText;
    }
}
