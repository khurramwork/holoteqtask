package com.techsmartsoft.holoteq.glide;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Glide module for Image loading and caching
 */
@GlideModule
public final class MyAppGlideModule extends AppGlideModule {
    // leave empty for now
}
