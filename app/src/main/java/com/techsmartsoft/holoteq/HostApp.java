package com.techsmartsoft.holoteq;

import android.app.Application;

public class HostApp extends Application {
    private static HostApp mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static HostApp getContext() {
        return mContext;
    }
}
