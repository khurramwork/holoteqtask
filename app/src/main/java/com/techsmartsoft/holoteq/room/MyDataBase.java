package com.techsmartsoft.holoteq.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.techsmartsoft.holoteq.pojo.ProductDemo;

/**
 * Room Database class with version information
 */
@Database(entities = {ProductDemo.class}, version = 1)
public abstract class MyDataBase extends RoomDatabase {
    private static MyDataBase instance;

    /**
     *  Singleton object for database
     * @param context
     * @return
     */
    public static MyDataBase getDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, MyDataBase.class, "HoloTeqDB").build();
        }
        return instance;
    }
    public abstract ProductDAO getProductDAO();

}

