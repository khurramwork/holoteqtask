package com.techsmartsoft.holoteq.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.techsmartsoft.holoteq.pojo.ProductDemo;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface ProductDAO {

    @Query("select * from ProductDemoTable order by productName ASC")
    Flowable<List<ProductDemo>> getItemList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addData(List<ProductDemo> productDemos);

    @Query("DELETE FROM ProductDemoTable WHERE rowid in (select rowid FROM ProductDemoTable LIMIT 1)")
    void delete();

    @Query("DELETE FROM ProductDemoTable WHERE productId=:productId")
    void delete(int productId);

}
