package com.techsmartsoft.holoteq;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.techsmartsoft.holoteq.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity {

    public static final String MESSAGE_STATUS = "MESSAGE_STATUS";
    public static final String IS_FIST_TIME = "IS_FIST_TIME";

    /**
     * Java = Done
     * Retrofit = Done
     * Room db = Done
     * Glide = Done
     * DiffUtil = Pending
     * Swipe to delete = Done
     * Implement a Work Manager = Done
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow();
        }
    }
}