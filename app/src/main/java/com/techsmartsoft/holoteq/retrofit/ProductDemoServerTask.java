package com.techsmartsoft.holoteq.retrofit;

import com.techsmartsoft.holoteq.pojo.ProductDemo;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * ProductDemoServerTask is calling getProducts Asynchronous call for service and send back update to UI thread with Rx java
 */
public class ProductDemoServerTask {
    public static void getProducts(final Callback<List<ProductDemo>> callback) {
        NetworkingUtils.getProductApiInstance()
                .getProducts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<ProductDemo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(List<ProductDemo> users) {
                        callback.returnResult(users);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.returnError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}

