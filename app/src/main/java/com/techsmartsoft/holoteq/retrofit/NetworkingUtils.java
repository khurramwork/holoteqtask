package com.techsmartsoft.holoteq.retrofit;

public  class NetworkingUtils{
    /**
     * Simply provide the API Interface to call the actual api with Retrofit
     */
    private static DemoAPIService userService;
    public static DemoAPIService getProductApiInstance() {
        if (userService == null)
            userService = RetrofitAdapter.getInstance().create(DemoAPIService.class);
        return userService;
    }
}
