package com.techsmartsoft.holoteq.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Retrofit Adapter Singleton class
 */
public class RetrofitAdapter {
    private static Retrofit retrofit;
    private static Gson gson;
    public static final String DOMAIN = "http://89.47.163.156:8080";
    private static final String SERVICE_PATH = "/OrderLayServices/api/";
    private static final String BASE_URL = DOMAIN+SERVICE_PATH;
    /**
     * Create singleton object for retrofit
     * @return
     */
    public static synchronized Retrofit getInstance() {

        if (retrofit == null) {
            if (gson == null) {
                gson = new GsonBuilder().setLenient().create();
            }
            /**
             * Added HTTP Logger for login purpose
             */
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();

        }

        return retrofit;
    }
}
