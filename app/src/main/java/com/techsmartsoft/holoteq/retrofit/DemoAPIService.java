package com.techsmartsoft.holoteq.retrofit;

import com.techsmartsoft.holoteq.pojo.ProductDemo;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.POST;

/**
 * Retrofit API interface
 */
public interface DemoAPIService {
    @POST("test/demo")
    Observable<List<ProductDemo>> getProducts();
}
