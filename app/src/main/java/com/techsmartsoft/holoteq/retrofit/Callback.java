package com.techsmartsoft.holoteq.retrofit;

/**
 * custom call back for continence using with retrofit results
 * @param <T>
 */
public abstract class Callback<T> {
    public abstract void returnResult(T t);
    public abstract void returnError(String message);
}
