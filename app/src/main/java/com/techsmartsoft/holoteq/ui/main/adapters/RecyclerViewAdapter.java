package com.techsmartsoft.holoteq.ui.main.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.techsmartsoft.holoteq.R;
import com.techsmartsoft.holoteq.glide.GlideApp;
import com.techsmartsoft.holoteq.pojo.ProductDemo;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import static com.techsmartsoft.holoteq.retrofit.RetrofitAdapter.DOMAIN;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private ArrayList<ProductDemo> data;
    private String CURRENCY="₹";
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtProductName,txtOldPrice,txtNewPrice,txtUnit;
        private ImageView imgProductImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgProductImage = itemView.findViewById(R.id.imgProductImage);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtOldPrice = itemView.findViewById(R.id.txtOldPrice);
            txtNewPrice = itemView.findViewById(R.id.txtNewPrice);
            txtUnit = itemView.findViewById(R.id.txtUnit);
        }
    }

    public RecyclerViewAdapter(ArrayList<ProductDemo> data, Context context) {
        this.context=context;
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ProductDemo productDemo = data.get(position);
        holder.txtProductName.setText(productDemo.getProductName());
        holder.txtOldPrice.setText(context.getString(R.string.old_price)+CURRENCY+productDemo.getDefaultOldPrice());
        holder.txtNewPrice.setText(context.getString(R.string.selling_price)+CURRENCY+productDemo.getDefaultNewPrice());
        holder.txtUnit.setText(context.getString(R.string.per)+" "+productDemo.getUnitText());
        /**
         * Demonstration of Simple use of glide library
         */
        GlideApp.with(context).load(DOMAIN+productDemo.getDefaultUrl()).placeholder(R.drawable.ic_launcher_background).into(holder.imgProductImage);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * this method is use to remove item from the list
     * @param position
     */
    public void removeItem(int position) {
        if(position<data.size()) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    /**
     * this method will use to restore the list item
     * @param item
     * @param position
     */
    public void restoreItem(ProductDemo item, int position) {
        data.add(position, item);
        notifyItemInserted(position);
    }

    public ArrayList<ProductDemo> getData() {
        return data;
    }

    public void setData(List<ProductDemo> newData) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new MyDiffCallback(newData,this.data));
        this.data.clear();
        this.data.addAll(newData);
        diffResult.dispatchUpdatesTo(this);

    }

    /**
     * Implementation of diff util for optimization
     */
    public class MyDiffCallback extends DiffUtil.Callback{
        List<ProductDemo> oldList;
        List<ProductDemo> newList;

        public MyDiffCallback(List<ProductDemo> newList, List<ProductDemo> oldList) {
            this.newList = newList;
            this.oldList = oldList;
        }

        @Override
        public int getOldListSize() {
            return oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).getProductId() == newList.get(newItemPosition).getProductId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).getProductName().equals(newList.get(newItemPosition).getProductName());
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            //you can return particular field for changed item.
            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }
}

