package com.techsmartsoft.holoteq.ui.main;

import android.content.Context;
import android.net.ConnectivityManager;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.techsmartsoft.holoteq.HostApp;
import com.techsmartsoft.holoteq.R;
import com.techsmartsoft.holoteq.pojo.ProductDemo;
import com.techsmartsoft.holoteq.retrofit.Callback;
import com.techsmartsoft.holoteq.retrofit.ProductDemoServerTask;
import com.techsmartsoft.holoteq.room.MyDataBase;
import com.techsmartsoft.holoteq.worker.MyWorker;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {
    private final MutableLiveData<String> uiMessages = new MutableLiveData<>();
    private final MutableLiveData<List<ProductDemo>> productLiveData = new MutableLiveData<>();
    private MyDataBase appDatabase;
    private PeriodicWorkRequest workRequest;

    public MainViewModel() {
        super();
        appDatabase = MyDataBase.getDatabase(HostApp.getContext());
    }

    /**
     * Fetch offline recode from sqlite db using room wrapper
     */
    public void getOfflineProductList() {
        appDatabase.getProductDAO().getItemList().subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(products -> {
                            if (products == null | products.size() == 0) {
                                uiMessages.postValue(HostApp.getContext().getString(R.string.offline_product_message));
                            }else{
                                productLiveData.postValue(products);
                            }
                        }, e ->
                                uiMessages.postValue(HostApp.getContext().getString(R.string.offline_exception_message))
                );
    }

    /**
     * Delete specific item from database with product id
     *
     * @param productId
     */
    public void deleteItem(int productId) {
        Completable.fromAction(() -> appDatabase.getProductDAO().delete(productId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    uiMessages.postValue("Item Deleted");
                }, throwable -> {
                    uiMessages.postValue("Exception in Delete :" + throwable.getMessage());
                });
    }


    /**
     * Hit server with retrofit and get the demo product list.
     */
    public void loadServerProducts() {
        if(!isOnline()) {
            uiMessages.postValue(HostApp.getContext().getString(R.string.internet_error) );
        }

        uiMessages.postValue(HostApp.getContext().getString(R.string.backend_server_data_message));
        ProductDemoServerTask.getProducts(new Callback<List<ProductDemo>>() {
            @Override
            public void returnResult(List<ProductDemo> users) {
                uiMessages.postValue(HostApp.getContext().getString(R.string.backend_server_message));
                storeOfflineProducts(users);
            }

            @Override
            public void returnError(String message) {
                uiMessages.postValue(HostApp.getContext().getString(R.string.backend_server_exception) + message);
            }
        });
    }

    /**
     * Store the server list into offline database
     *
     * @param productDemos
     */
    public void storeOfflineProducts(List<ProductDemo> productDemos) {
        Completable.fromAction(() -> appDatabase.getProductDAO().addData(productDemos))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    uiMessages.postValue(HostApp.getContext().getString(R.string.all_record_inserted));
                }, throwable -> {
                    uiMessages.postValue(HostApp.getContext().getString(R.string.insertion_error) + throwable.getMessage());
                });
    }

    /**
     * Schedule the WorkManager for specific interval to execute in background
     *
     * @param fragment
     */
    public void scheduleWorkManager(MainFragment fragment) {
        WorkManager mWorkManager = WorkManager.getInstance(fragment.getContext());
        workRequest = new PeriodicWorkRequest.Builder(MyWorker.class, 60, TimeUnit.MINUTES).build();
        mWorkManager.getWorkInfoByIdLiveData(workRequest.getId()).observe(fragment, workInfo -> {
            if (workInfo != null) {
                //NO Requirements, We can also post update to UI,
                //WorkInfo.State state = workInfo.getState();
                // uiMessages.postValue("WorkManager Update: " + state.name());
            }
        });
        mWorkManager.enqueue(workRequest);
    }

    public MutableLiveData<String> getUiMessages() {
        return uiMessages;
    }

    public MutableLiveData<List<ProductDemo>> getProductLiveData() {
        return productLiveData;
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) HostApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}