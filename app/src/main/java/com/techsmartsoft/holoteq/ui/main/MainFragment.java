package com.techsmartsoft.holoteq.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.techsmartsoft.holoteq.R;
import com.techsmartsoft.holoteq.pojo.ProductDemo;
import com.techsmartsoft.holoteq.ui.main.adapters.RecyclerViewAdapter;
import com.techsmartsoft.holoteq.ui.main.adapters.SwipeToDeleteCallback;

import java.util.ArrayList;

import static com.techsmartsoft.holoteq.MainActivity.IS_FIST_TIME;

public class MainFragment extends Fragment {
    public static MainFragment newInstance() {
        return new MainFragment();
    }
    private MainViewModel mViewModel;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter mAdapter;
    private ArrayList<ProductDemo> demoArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        view.findViewById(R.id.fabRefreshButton).setOnClickListener(view1 -> mViewModel.loadServerProducts());
        /**
         * Load recylerView
         */
        populateRecyclerView();
        /**
         * implementation for swipe to delete items
         */
        enableSwipeToDelete();
    }

    /**
     * Bind swipe to delete Callback with recylerview
     */
    private void enableSwipeToDelete() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(getContext()) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                int position = viewHolder.getAdapterPosition();
                ProductDemo item = mAdapter.getData().get(position);
                mAdapter.removeItem(position);
                mViewModel.deleteItem(item.getProductId());
            }
        };
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);
    }

    /**
     * bind adapter with recylerView
     */
    private void populateRecyclerView() {
        mAdapter = new RecyclerViewAdapter(demoArrayList, getContext());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        /**
         * View Model Post UI messages
         */
        mViewModel.getUiMessages().observe(this, message -> {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        });
        /**
         * Load product from Database
         */
        mViewModel.getProductLiveData().observe(this, products -> {
            /**
             * set data internally uses Diff Util for optimization and performance
             */
            mAdapter.setData(products);
            getActivity().getPreferences(Context.MODE_PRIVATE).edit().putBoolean(IS_FIST_TIME, true).commit();
        });

        /**
         * Only First time call the server
         */
        if (!getActivity().getPreferences(Context.MODE_PRIVATE).getBoolean(IS_FIST_TIME, false)) {
            mViewModel.loadServerProducts();
            /**
             * Schedule Work manager
             */
            mViewModel.scheduleWorkManager(this);
        }
        mViewModel.getOfflineProductList();
    }


}